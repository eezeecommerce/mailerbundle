FOSUserBundle Registration Email
================================

To override the password reset email:

```yaml
# app/config/config.yml

service:
    mailer: fos_user.mailer.twig_swift
resetting:
    email:
        template: eezeecommerceMailerBundle:Email:password_reset.email.twig
```

LexikMailer
===========

Installation
============

Register the bundle with your kernel:
```php
<?php
$bundles = array(
    new eezeecommerce\UserBundle\eezeecommerceMailerBundle(),
    new Lexik\Bundle\MailerBundle\LexikMailerBundle(),
);
```

________________________

Configuration
=============

```yaml
# app/config/config.yml
lexik_mailer:
    admin_email: %your_admin_email% # Admin email used to notify email template errors
```

Change the default layout used with email templates:
```yaml
# app/config/config.yml
lexik_mailer:
    base_layout: "eezeecommerceMailerBundle:template.html.twig"
```

The bundle provides a GUI to edit the templates, to access these pages just load the routing file

```yaml
# app/config/routing.yml
LexikMailerBundle:
    resource: "@LexikMailerBundle/Resources/config/routing.xml"
    prefix: /admin
```

Usage
=====

If you want the service to automatically get the name and email address of the recipient
```php
<?php

use Lexik\Bundle\MailerBundle\Mapping\Annotation as Mailer;

class User
{
    /**
    * @Mailer\Name()
    *
    * @return string
    */
    public function getName()
    {
        return $this->name;
    }

    /**
    * @Mailer\Address()
    *
    * @return string
    */
    public function getEmail()
    {
        return $this->email;
    }
}
```

Or implement Email DestinationInterface

```php
<?php

use Lexik\Bundle\MailerBundle\Model\EmailDestinationInterface;

class User implements EmailDestinationInterface
{
    /**
    * @return string
    */
    public function getRecipientName()
    {
        return $this->name;
    }

    /**
    * @return string
    */
    public function getRecipientEmail()
    {
        return $this->email;
    }
}
```


Once you have created the template you can use swiftmailer to send the email:

```php
<?php

$to = "an@email.address";
$params = array("name" => "foo");
$locale = "en";

$message = $this->get('lexik_mailer.message_factory')->get('template-name', $to, $params, $locale);

$this->get('mailer')->send($message);

```

